module.exports = (sequelize, type) => {
  return sequelize.define('ordermodifier', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    qty: type.FLOAT,
    instruction: type.STRING

  })
}