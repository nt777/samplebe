module.exports = (sequelize, type) => {
  return sequelize.define('table', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    waiterid: type.STRING,
    tableid: type.INTEGER,
	tablestatus: type.INTEGER,
	tabletotal: type.FLOAT
	})
}