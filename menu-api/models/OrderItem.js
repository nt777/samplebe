module.exports = (sequelize, type) => {
  return sequelize.define('orderitem', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    qty: type.FLOAT,
    instruction: type.STRING
  })
}