module.exports = (sequelize, type) => {
  return sequelize.define('menumodifier', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: type.STRING,
    price: type.FLOAT,
    tax: type.FLOAT,
    discount: type.FLOAT
  })
}