module.exports = (sequelize, type) => {
  return sequelize.define('menucat', {
    id: {
      type: type.INTEGER,
      primaryKey: true
    },
    Category_Id: type.INTEGER,
    Category_Code: type.STRING,
    Category_Name: type.STRING,
    Type: type.STRING,
    Remarks: type.STRING,
    TaxId: type.INTEGER,
    DiscountId: type.INTEGER
  })
}