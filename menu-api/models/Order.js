module.exports = (sequelize, type) => {
  return sequelize.define('order', {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    waiterid: type.STRING,
    ordertype: type.STRING,
	  orderstatus: type.STRING,
    totalamt: type.FLOAT,
    totaltax: type.FLOAT,
    totaldiscount: type.FLOAT
	})
}