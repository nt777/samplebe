'use strict';
const connectToDatabase = require('./db') // initialize connection

// simple Error constructor for handling HTTP error codes
function HTTPError (statusCode, message) {
  const error = new Error(message)
  error.statusCode = statusCode
  return error
}

module.exports.healthCheck = async () => {
  await connectToDatabase()
  console.log('Connection successful.')
  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Connection successful.' })
  }
}

module.exports.getMenus = async () => {
  try {
    const { MenuCat, MenuItem, MenuModifier } = await connectToDatabase()
    const menucats = await MenuCat.findAll({ include: {model: MenuItem,
      include: [MenuModifier] 
      }
    })
    return {
      statusCode: 200,
      body: JSON.stringify(menucats)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err+'Could not fetch the items.'
    }
  }
}

module.exports.getTables = async () => {
  try {
    const { Table } = await connectToDatabase()
    const tables = await Table.findAll()
    return {
      statusCode: 200,
      body: JSON.stringify(tables)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err+'Could not fetch the tables.'
    }
  }
}

module.exports.getOrders = async () => {
  try {
    const { Order } = await connectToDatabase()
    const orders = await Order.findAll()
    return {
      statusCode: 200,
      body: JSON.stringify(orders)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err+'Could not fetch the orders.'
    }
  }
}

module.exports.createOrder = async (event) => {
  try {
    const { Order, OrderItem, OrderModifier } = await connectToDatabase()
    const order = await Order.create(JSON.parse(event.body), { include: {model: OrderItem,
      include: [OrderModifier] 
      }})
    return {
      statusCode: 200,
      body: JSON.stringify(order)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err+'Could not create the order.'
    }
  }
}

module.exports.getOrder = async (event) => {
  try {
    const { Order } = await connectToDatabase()
    const order = await Order.findByPk(event.pathParameters.id)
    if (!order) throw new HTTPError(404, `Order with id: ${event.pathParameters.id} was not found`)
    return {
      statusCode: 200,
      body: JSON.stringify(order)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err.message || 'Could not fetch the order.'
    }
  }
}

module.exports.updateOrder = async (event) => {
  try {
    const input = JSON.parse(event.body)
    const { Order } = await connectToDatabase()
    const order = await Order.findByPk(event.pathParameters.id)
    if (!order) throw new HTTPError(404, `Order with id: ${event.pathParameters.id} was not found`)
   // if (input.title) order.title = input.title
   // if (input.description) order.description = input.description
   order.orderStatus=input.orderStatus;
   order.totalamt=input.totalamt;
   order.totaltax=input.totaltax;
    await order.save()
    return {
      statusCode: 200,
      body: JSON.stringify(order)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err.message || 'Could not update the Order.'
    }
  }
}

module.exports.destroyOrder = async (event) => {
  try {
    const { Order } = await connectToDatabase()
    const order = await Order.findById(event.pathParameters.id)
    if (!order) throw new HTTPError(404, `Order with id: ${event.pathParameters.id} was not found`)
    await order.destroy()
    return {
      statusCode: 200,
      body: JSON.stringify(order)
    }
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: err.message || 'Could destroy fetch the Order.'
    }
  }
}