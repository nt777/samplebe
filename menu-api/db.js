const Sequelize = require('sequelize')
const OrderModel = require('./models/Order')
const TableModel = require('./models/Table')
const MenuCatModel = require('./models/MenuCat')
const MenuItemModel = require('./models/MenuItem')
const MenuModifierModel = require('./models/MenuModifier')
const OrderItemModel = require('./models/OrderItem')
const OrderModifierModel = require('./models/OrderModifier')

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    dialect: 'mysql',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
  }
)

const Order = OrderModel(sequelize, Sequelize)
const Table = TableModel(sequelize, Sequelize)
const MenuCat = MenuCatModel(sequelize, Sequelize)
const MenuItem = MenuItemModel(sequelize, Sequelize)
const MenuModifier = MenuModifierModel(sequelize, Sequelize)
const OrderItem = OrderItemModel(sequelize, Sequelize)
const OrderModifier = OrderModifierModel(sequelize, Sequelize)

MenuCat.hasMany(MenuItem)
MenuItem.hasMany(MenuModifier)
//
Order.hasMany(OrderItem)
OrderItem.hasMany(OrderModifier)
OrderItem.belongsTo(MenuItem)
OrderModifier.belongsTo(MenuModifier)
//      MenuModifier.belongsTo(OrderModifier)
//        Table.belongsTo(Order)
const Models = { Order, OrderItem, OrderModifier, Table, MenuCat, MenuItem, MenuModifier }
const connection = {}

module.exports = async () => {
  if (connection.isConnected) {
    console.log('=> Using existing connection.')
    return Models
  }

  await sequelize.sync()
  await sequelize.authenticate()
  connection.isConnected = true
  console.log('=> Created a new connection.')
  return Models
}     